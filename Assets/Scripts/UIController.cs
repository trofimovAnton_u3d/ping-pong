﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIController : MonoBehaviour
{
    public Text scoreText;
    public Text highScoreText;
    public GameObject startPanel;
    public GameObject gameOverPanel;
    public static UIController instance;

    public void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ShowStartUI();
    }

    public void UpdateScore(int val)
    {
        scoreText.text ="Score: " +  val.ToString();
    }

    public void UpdateHighScore(int val)
    {
        highScoreText.text = "High score: " + val.ToString();
    }

    public void GameOver()
    {
        gameOverPanel.gameObject.SetActive(true);
    }


    public void ShowStartUI()
    {
        startPanel.gameObject.SetActive(true);
    }
}
