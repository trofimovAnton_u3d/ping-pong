﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GameData
{
    public int highScores = 0;
    public Color ballColor = Color.white;

    public static void Save(string key, GameData data)
    {
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));
    }

    public static GameData Load(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return (JsonUtility.FromJson<GameData>(PlayerPrefs.GetString(key)));
        }
        else
        {
            GameData data = new GameData();
            Save(key, data);
            return data;
        }
    }
}
