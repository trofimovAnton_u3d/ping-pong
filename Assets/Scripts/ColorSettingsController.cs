﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ColorSettingsController : MonoBehaviour
{
    public Slider r_slider;
    public Slider g_slider;
    public Slider b_slider;

    public Renderer renderer;
    Color currentColor;
    void Update()
    {
        currentColor.r = r_slider.value;
        currentColor.g = g_slider.value;
        currentColor.b = b_slider.value;
        currentColor.a = 1f;
        renderer.material.color = currentColor;
    }

    public void SaveColor()
    {
        MainGameController.instance.SetBallColor(currentColor);
        MainGameController.instance.SaveCurrentGameData();
    }
}
