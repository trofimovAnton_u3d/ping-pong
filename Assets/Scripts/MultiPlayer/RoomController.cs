﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class RoomController : MonoBehaviourPunCallbacks
{

    public PlatformController platfromPrefab;
    public BallController ball;
    RoomOptions roomOptions = new RoomOptions();
    public Vector3 firstPlayerPosition;
    public Vector3 secondPlayerPosition;
    public Camera mainCam;

    PlatformController myPlatfrom;

    public override void OnJoinedRoom()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            MultiPlayerUIController.instance.ActiveStausPanel(false);
            mainCam.transform.eulerAngles = new Vector3(90, 180, 0);
            myPlatfrom = PhotonNetwork.Instantiate(platfromPrefab.gameObject.name, secondPlayerPosition, platfromPrefab.transform.rotation).GetComponent<PlatformController>();
            myPlatfrom.invertInput = true;
        }
        else if (PhotonNetwork.IsMasterClient)
        {
            MultiPlayerUIController.instance.UpdateStatus("wait for second player");
            mainCam.transform.eulerAngles = new Vector3(90, 0, 0);
            myPlatfrom = PhotonNetwork.Instantiate(platfromPrefab.gameObject.name, firstPlayerPosition, platfromPrefab.transform.rotation).GetComponent<PlatformController>(); ;
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            MultiPlayerUIController.instance.ActiveStausPanel(false);
            MultiPlayerGameController.instance.Init(
                PhotonNetwork.Instantiate(ball.gameObject.name, ball.transform.position, ball.transform.rotation).GetComponent<BallController>());
            MultiPlayerGameController.instance.RestartGame();
        }
    }


    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        MultiPlayerUIController.instance.UpdateStatus("join to room error");
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (newMasterClient == PhotonNetwork.LocalPlayer)
        {
            MultiPlayerUIController.instance.ActiveGameOverPanel(false);
            MultiPlayerUIController.instance.ActiveStausPanel(true);
            MultiPlayerUIController.instance.UpdateStatus("wait for second player");
            mainCam.transform.eulerAngles = new Vector3(90, 0, 0);
            myPlatfrom.transform.position = firstPlayerPosition;
            myPlatfrom.invertInput = false;
            myPlatfrom.Init();
        }
    }
}
