﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class ConnectionController : MonoBehaviourPunCallbacks
{
    RoomOptions roomOptions = new RoomOptions();
    public string debugRoomName = "testRoom";
    void Start()
    {
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 2;
        MultiPlayerUIController.instance.ActiveStausPanel(true);
        MultiPlayerUIController.instance.UpdateStatus("Connecting...");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        MultiPlayerUIController.instance.UpdateStatus("Connected");
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.NickName = "player_" + Random.Range(1000, 9999999).ToString();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinOrCreateRoom(debugRoomName, roomOptions, TypedLobby.Default);
       
    }

    public void Disconect()
    {
        PhotonNetwork.Disconnect();
    }


}
