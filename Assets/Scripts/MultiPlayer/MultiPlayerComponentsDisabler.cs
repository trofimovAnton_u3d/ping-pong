﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

[RequireComponent(typeof(PhotonView))]
public class MultiPlayerComponentsDisabler : MonoBehaviour
{
    PhotonView photonView;
    public List<MonoBehaviour> componentsToDisable = new List<MonoBehaviour>();

    void Start()
    {
        photonView = GetComponent<PhotonView>();
        if (!photonView.IsMine)
        {
            componentsToDisable.ForEach(c => c.enabled = false);
        }
    }
}
