﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class MultiPlayerBallCollisionResolver : MonoBehaviour, ICollisionResolver
{
    PhotonView photonView;

    void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void ResolveCollision(string tag)
    {
        switch (tag)
        {
            case "deadZone":
                MultiPlayerGameController.instance.GameOver();
                break;

            case "platform":
                MultiPlayerGameController.instance.AddScore(1);
                break;
        }
    }

}
