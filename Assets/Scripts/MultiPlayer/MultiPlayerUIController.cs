﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
public class MultiPlayerUIController : MonoBehaviour
{
    public Text scoreText;
    public GameObject gameOverPanel;
    public Text gameOverText;
    public Text statusText;
    public GameObject statusPanel;

    public static MultiPlayerUIController instance;
    public void Awake()
    {
        instance = this;
    }

    public void UpdateScore(int val)
    {
        scoreText.text = "Score: " + val.ToString();
    }

    public void GameOver()
    {
        ActiveGameOverPanel(true);
 
        if (PhotonNetwork.IsMasterClient)
        {
            gameOverText.text = "Game over! Tap to restart!";
        }
        else
        {
            gameOverText.text = "Game over! Wait for restart";
        }
    }

    public void UpdateStatus(string text)
    {
        statusText.text = text;
    }

    public void ActiveStausPanel(bool active)
    {
        statusPanel.gameObject.SetActive(active);
    }

    public void ActiveGameOverPanel(bool active)
    {
        gameOverPanel.gameObject.SetActive(active);
    }

}
