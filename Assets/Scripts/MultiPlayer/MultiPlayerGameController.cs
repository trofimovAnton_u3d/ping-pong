﻿using UnityEngine;
using Photon.Pun;
public class MultiPlayerGameController : MonoBehaviour
{
    PhotonView photonView;
    public int score = 0;
    public static MultiPlayerGameController instance;
    BallController ballController;
    private void Awake()
    {
        instance = this;
        photonView = GetComponent<PhotonView>();
    }

    public void Init(BallController ball)
    {
        ballController = ball;
    }
    public void RestartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            score = 0;
            ballController.transform.position = new Vector3(0, 0, 0);
            ballController.direction = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
            ballController.move = true;
            MultiPlayerUIController.instance.UpdateScore(score);
            photonView.RPC("StartGame_RPC", RpcTarget.All);
        }
    }

    [PunRPC]
    public void StartGame_RPC()
    {
        MultiPlayerUIController.instance.ActiveGameOverPanel(false);
    }


    public void GameOver()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            ballController.move = false;
            photonView.RPC("GameOver_RPC", RpcTarget.All);
        }
    }

    [PunRPC]
    public void GameOver_RPC()
    {
        MultiPlayerUIController.instance.GameOver();
    }

    public void AddScore(int val)
    {
        score += val;
        photonView.RPC("SetScore_RPC", RpcTarget.All, score);
    }

    [PunRPC]
    public void SetScore_RPC(int val)
    {
        score = val;
        MultiPlayerUIController.instance.UpdateScore(score);
    }
}
