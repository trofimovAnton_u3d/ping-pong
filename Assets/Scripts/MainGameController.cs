﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameController : MonoBehaviour
{
    public static MainGameController instance;
    public BallController ball;
    int score = 0;
    string gameDataPPKey = "gameData";
    public GameData currentData;
    public float minScale = 0.5f;
    public float maxScale = 3f;
    public float minSpeed = 0.1f;
    public float maxSpeed = 0.5f;
    public List<PlatformController> platforms = new List<PlatformController>();
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        currentData = GameData.Load(gameDataPPKey);
        ReinitGame();
    }
    public void AddScore(int val)
    {
        score += val;
        UIController.instance.UpdateScore(score);
    }

    public void SetBallColor(Color color)
    {
        currentData.ballColor = color;
        AcceptBallColor(color);
    }
    public void AcceptBallColor(Color color)
    {
        ball.SetColor(color);
    }

    public void GameOver()
    {
        ball.move = false;
        SetPlatformsMove(false);
        if (currentData.highScores < score)
        {
            currentData.highScores = score;
            SaveCurrentGameData();
        }
        UIController.instance.GameOver();
    }

    void SetPlatformsMove(bool move)
    {
        platforms.ForEach(p => p.move = move);
    }

    public void SaveCurrentGameData()
    {
        GameData.Save(gameDataPPKey, currentData);
    }

    [ContextMenu("reinit game")]
    public void ReinitGame()
    {
        score = 0;
        ball.ReInit(Vector3.zero,
             new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized,
             Random.Range(minSpeed, maxSpeed),
             Random.Range(minScale, maxScale));
        SetPlatformsMove(false);
        AcceptBallColor(currentData.ballColor);
        UIController.instance.ShowStartUI();
        UIController.instance.UpdateHighScore(currentData.highScores);
        UIController.instance.UpdateScore(score);
    }
    [ContextMenu("start game")]
    public void StartGame()
    {
        SetPlatformsMove(true);
        ball.move = true;
    }


}
