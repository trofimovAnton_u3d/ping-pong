﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollisionResolver
{
    void ResolveCollision(string tag);
}
public class CollisionResolver : MonoBehaviour, ICollisionResolver
{
    public void ResolveCollision(string tag)
    {
        switch (tag)
        {
            case "deadZone":
                MainGameController.instance.GameOver();
                break;

            case "platform":
                MainGameController.instance.AddScore(1);
                break;
        }
    }
}
