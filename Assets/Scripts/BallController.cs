﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallController : MonoBehaviour
{
    public Vector3 direction;
    Rigidbody rb;
    public bool move = true;
    public float currentSpeed = 1;
    public float currentScale;
    public Renderer rend;
    ICollisionResolver collisionResolver;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        collisionResolver = GetComponent<ICollisionResolver>();
    }

    private void Update()
    {
        if (move)
        {
            rb.MovePosition(transform.position + (direction * currentSpeed));
        }
    }


    public void SetColor(Color color)
    {
        rend.material.color = color;
    }
    [ContextMenu("reinit")]
    public void ReInit(Vector3 startPos, Vector3 startDirection,  float speed, float scale)
    {
        move = false;
        transform.position = startPos;
        direction = startDirection;
        this.currentScale = scale;
        currentSpeed = speed;
        transform.localScale = new Vector3(scale, scale, scale);
    }

    public void OnCollisionEnter(Collision collision)
    {
        direction = Vector3.Reflect(direction, collision.contacts[0].normal);
        collisionResolver?.ResolveCollision(collision.gameObject.tag);
    }
}
