﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenuController : MonoBehaviour
{
    public void GoToSinglePlayer()
    {
        SceneManager.LoadScene("SingleplayerScene");
    }

    public void GoToMultiplayer()
    {
        SceneManager.LoadScene("MultiplayerScene");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
