﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public float speed;
    Vector3 platfromPosition;
    public float boundary;
    float screenWidthHalf;
    public bool move = false;
    public bool invertInput = false;
    void Start()
    {
        Init();
    }

    public void Init()
    {
        platfromPosition = gameObject.transform.position;
        screenWidthHalf = Screen.width / 2f;
    }

    void Update()
    {
        if (move)
        {
#if UNITY_EDITOR
            PCInput();
#else
            MobileInput();
#endif
        }

    }

    void PCInput()
    {
        if (Input.GetMouseButton(0))
        {
            Move((Input.mousePosition.x - screenWidthHalf) / (screenWidthHalf));
        }
    }

    void MobileInput()
    {
        if (Input.touchCount>0)
        {
            Move((Input.GetTouch(0).position.x - screenWidthHalf) / (screenWidthHalf));
        }
    }


    public void Move(float val)
    {
        val = invertInput ? -val : val;
        platfromPosition.x = val * boundary;
        transform.position = Vector3.MoveTowards(transform.position, platfromPosition, Time.deltaTime * speed);
    }

}
